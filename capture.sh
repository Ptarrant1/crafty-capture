#/bin/bash

VERSION="1.0 Alpha"
NAME="Crafty Capture"

PWD=$(pwd)
USER=$(whoami)
HOST=$(hostname)

PID=$1

EVIDENCE_DIR="${PWD}/${PID}_evidence"
EVIDENCE_FILE="$EVIDENCE_DIR/evidence.log"

pretty_print()
{
    HEADER="\e[36m"
    INFO="\e[1m [+] INFO \t "
    ERROR="\e[91m [+] ERROR \t"
    YELLOW="\e[93m [+] INFO \t"
    NORMAL="\e[0m"
    

    if [ $1 == "HEADER" ];then 
        echo -e $HEADER $2 "\e[0m"

    elif [ $1 == "ERROR" ];then 
        echo -e $ERROR $2 "\e[0m"

    elif [ $1 == "YELLOW" ];then 
        echo -e $YELLOW $2 "\e[0m"
    
    elif [ $1 == "INFO" ];then 
        echo -e $INFO $2 "\e[0m"
    else
        echo $2
    fi

}

sudo_check()
{
    if [[ $EUID -ne 0 ]]; then
        echo "This script must be run as root" 
        exit 1
    fi
}


args_check()
{
    if [ $# -eq 0 ]
    then
        pretty_print  ERROR "No arguments supplied"
        pretty_print  ERROR "To capture data on process 12345 type the string below - note SUDO / ROOT is required"
        pretty_print  ERROR "./capture.sh 123456"

        exit 1
    fi
}

header()
{
    echo -e "\n"
    echo -e "==========================================="
    pretty_print HEADER "\t Welcome to $NAME"
    pretty_print HEADER "\t \t \t \t   v$VERSION"
    echo -e "===========================================\n"
    echo -e "\n"
}

sudo_check
args_check $1

header


pretty_print INFO "Capturing PID $PID"

#check to see if the PID still exists
EXISTS=`ps p $PID | tail -n 1 | wc -l`

if [ $EXISTS == 1 ]; then
    pretty_print INFO "Process $PID is still running"
else
    pretty_print ERROR "Process $PID is not running/found..."
    exit 1
fi

mkdir -p $EVIDENCE_DIR
touch $EVIDENCE_FILE
pretty_print YELLOW "Evidence will be stored in $EVIDENCE_DIR"


PSOWNER=`ps -o ruser -p $PID | tail -n 1`
CMD=`strings /proc/$PID/cmdline`
PROC_CWD=`readlink -f /proc/$PID/cwd`
PROC_BINARY=`readlink -f /proc/$PID/exe`



echo "Gathering Evidence for $PID" > $EVIDENCE_FILE

pretty_print INFO "Gathering Evidence for $PID"

echo -e "\n================================================\n" >> $EVIDENCE_FILE
echo -e "System Name" >> $EVIDENCE_FILE
hostname >> $EVIDENCE_FILE

echo -e "\n================================================\n" >> $EVIDENCE_FILE
echo -e "System Release Info" >> $EVIDENCE_FILE
uname -a >> $EVIDENCE_FILE

echo -e "\n================================================\n" >> $EVIDENCE_FILE

if test -f "/etc/os-release"; then
    cat /etc/os-release >> $EVIDENCE_FILE 2>/dev/null

elif test -f "/etc/lsb-release"; then
    cat /etc/lsb-release >> $EVIDENCE_FILE 2>/dev/null
fi 

echo -e "\n================================================\n" >> $EVIDENCE_FILE

echo -e "Timezone" >> $EVIDENCE_FILE
cat /etc/localtime >> $EVIDENCE_FILE 2>/dev/null

echo -e "\n================================================\n" >> $EVIDENCE_FILE

echo "Owner of $PID is: $PSOWNER" >> $EVIDENCE_FILE
echo "Current Working Dir for $PID is: $PROC_CWD" >> $EVIDENCE_FILE
echo -e "\n================================================\n" >> $EVIDENCE_FILE

echo "PS Info of $PID" >> $EVIDENCE_FILE
ps uwwwp $PID >> $EVIDENCE_FILE
echo -e "\n================================================\n" >> $EVIDENCE_FILE

echo "Binary of $PID" >> $EVIDENCE_FILE
echo $PROC_BINARY >> $EVIDENCE_FILE
echo -e "\n================================================\n" >> $EVIDENCE_FILE

pretty_print INFO "Dumping Last Login (absolute last time) for $PSOWNER"
echo "Last Login of $PSOWNER" >> $EVIDENCE_FILE
lastlog | grep -i $PSOWNER >> $EVIDENCE_FILE
echo -e "\n================================================\n" >> $EVIDENCE_FILE


pretty_print INFO "Dumping all Last Logins for $PSOWNER"
last | grep -i $PSOWNER >> $EVIDENCE_DIR/last_logins.log

pretty_print INFO "Dumping process tree for $PID"
pstree -l -p -s $PID >> $EVIDENCE_DIR/pstree.log

pretty_print INFO "Gathering Open Files Evidence for $PID"
lsof -a -p $PID >> $EVIDENCE_DIR/lsof.log 2>/dev/null

pretty_print INFO "Gathering Environment Variables for $PID"
cat /proc/$PID/environ >> $EVIDENCE_DIR/environment.log 2>/dev/null

pretty_print INFO "Gathering Status for $PID"
cat /proc/$PID/status >> $EVIDENCE_DIR/{$PID}_status.log 2>/dev/null

pretty_print INFO "Gathering Memory Maps for $PID"
cat /proc/$PID/maps >> $EVIDENCE_DIR/{$PID}_maps.log 2>/dev/null

pretty_print INFO "Gathering Sudoers info for $HOST"
cat /etc/sudoers >> $EVIDENCE_DIR/sudoers.log 2>/dev/null

pretty_print INFO "Gathering hostfile info for $HOST"
cat /etc/hosts >> $EVIDENCE_DIR/hosts.log 2>/dev/null

# GCORE
if ! command -v gcore &> /dev/null; then
    pretty_print ERROR "gcore could not be found! Skipping Memory Capture"

else
    pretty_print INFO "Capturing Memory for $PID - this might take a moment"
    sudo gcore -a $PID > /dev/null 2>/dev/null
    sudo mv core.$PID $EVIDENCE_DIR/$PID.bin
    pretty_print YELLOW "Memory File Captured as ${EVIDENCE_DIR}/{$PID}.bin"
fi

# ldd
if ! command -v ldd &> /dev/null; then
    pretty_print ERROR "ldd could not be found! Skipping Linked Libraries Capture"

else
    pretty_print INFO "Capturing Linked Libraries for $PID"
    ldd $PROC_BINARY > $EVIDENCE_DIR/ldd.log 2>/dev/null
fi

# ltrace
if ! command -v ltrace &> /dev/null; then
    pretty_print ERROR "ltrace could not be found! Skipping Functions Capture"

else
    pretty_print INFO "Capturing functions for $PID"
    ltrace $PROC_BINARY > $EVIDENCE_DIR/ltrace.log 2>/dev/null
fi

# hexdump
if ! command -v hexdump &> /dev/null; then
    pretty_print ERROR "hexdump could not be found! Skipping Functions Capture"

else
    pretty_print INFO "Capturing hexdump for $PID - this might take a moment"
    hexdump -C $PROC_BINARY > $EVIDENCE_DIR/hexdump.log 2>/dev/null
fi

# readelf
if ! command -v readelf &> /dev/null; then
    pretty_print ERROR "readelf could not be found! Skipping ELF data Capture"
else
    pretty_print INFO "Capturing ELF headers for $PID"
    readelf -h $PROC_BINARY > $EVIDENCE_DIR/headers.log 2>/dev/null
fi

pretty_print INFO "Dumping SSH Logs for $PSOWNER"
journalctl --unit ssh >> $EVIDENCE_FILE 2>/dev/null

pretty_print INFO "Gathering Extended System Data"
cp -a /etc/passwd $EVIDENCE_DIR/passwd.log 2>/dev/null
cp -a /etc/group $EVIDENCE_DIR/group.log 2>/dev/null
cp -a /var/log/auth.log $EVIDENCE_DIR/auth.log 2>/dev/null
cp -a /var/log/secure.log $EVIDENCE_DIR/secure.log 2>/dev/null
cp -a /var/log/audit/audit.log $EVIDENCE_DIR/audit.log 2>/dev/null
cp -a /home/$PSOWNER/.bash_history $EVIDENCE_DIR/bash_history.log 2>/dev/null
dmesg > $EVIDENCE_DIR/dmesg.log 2>/dev/null

pretty_print INFO "Gathering Audit Logs"
utmpdump /var/log/wtmp > $EVIDENCE_DIR/wtmp.log 2>/dev/null
utmpdump /var/run/utmp > $EVIDENCE_DIR/utmp.log 2>/dev/null
utmpdump /var/log/btmp > $EVIDENCE_DIR/btmp.log 2>/dev/null

pretty_print INFO "Gathering Cron Jobs"
crontab -l > $EVIDENCE_DIR/root_cron.log 2>/dev/null
crontab -u $PSOWNER -l > $EVIDENCE_DIR/${PSOWNER}_cron.log 2>/dev/null

pretty_print INFO "Gathering Systemctl Timers"
systemctl list-timers --all > $EVIDENCE_DIR/systemctl_timers.log

systemctl list-unit-files > $EVIDENCE_DIR/systemctl_units.log
systemctl list-unit-files | grep -i enabled > $EVIDENCE_DIR/systemctl_units_enabled.log
systemctl list-unit-files | grep -i running > $EVIDENCE_DIR/systemctl_units_running.log

pretty_print INFO "Gathering file info in /tmp"
find /tmp -type f -exec file -- {} + > $EVIDENCE_DIR/tmp_file_data_all.log
cat $EVIDENCE_DIR/tmp_file_data_all.log | grep ELF > $EVIDENCE_DIR/tmp_file_data_ELF.log


pretty_print YELLOW "Evidence Collection Complete."
pretty_print YELLOW "Your files are in $EVIDENCE_DIR"