# Crafty Capture

## What's it do?
A script to gather all kinds of evidence about a process. Primarily used to gather as much data about a rogue process as possible.
This will not only dump data about the process but also grab ssh logs, audit logs, and other useful information. The script also grabs some data about the user who the process is running under as well. The script will also give you some data about the system. This is very useful for Incident Response. 

## Requirements:
This script must be ran as root. This script will try to use the following programs which are normally installed with Linux.
* Hexdump
* gcore
* pstree
* lsof
* ldd
* ltrace
* readelf
* journalctl (systemd)
* systemctl (systemd)
* dmesg
* untmpdump

## Where is my evidence?
Your evidence will be placed in a new directory called PID_evidence. For example, if investigating 1496 a new directory called 1496_evidence is where you evidence will be. This directory is always created as a child of PWD.

## Screenshot:
![screenshot](screenshot.png)
